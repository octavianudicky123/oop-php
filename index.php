<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$domba = new Animal("shaun");
echo "Name: " . $domba->name . "<br>";
echo "legs: " . $domba->legs . "<br>";
echo "cold blooded: " . $domba->coldblooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "legs: " . $kodok->legs . "<br>";
echo "cold blooded: " . $kodok->coldblooded . "<br>";
echo "Jump: " . $kodok->jump();

$sungkong = new Ape("kera sakti");
echo "Name: " . $sungkong->name . "<br>";
echo "legs: " . $sungkong->legs . "<br>";
echo "cold blooded: " . $sungkong->coldblooded . "<br>";
echo "Yell: " . $sungkong->yell();
